#include <stdio.h>
#include <string.h>

#include "big.h"

int main(int argc, char **argv) {
    puts("Hello World!");
    
    if(argc <= 2) {
        printf("Usage: A B (A and B both being numbers)\n");
        return 0;
    }

    char out[4096];
    memset(out, 0, 4096);

    BigInt *a = bigint(argv[1]);
    BigInt *b = bigint(argv[2]);
    BigInt *c = bigint_add(a, b);
    bigint_str(c, out, 4095);
    printf("'%s' + '%s' = '%s'\n", argv[1], argv[2], out);

    size_t block = 0, shift = 0;
    bigint_highest_digit(a, &block, &shift);
    printf("A highest {block, shift} = { %lu, %lu }\n", block, shift);
    bigint_highest_digit(b, &block, &shift);
    printf("B highest {block, shift} = { %lu, %lu }\n", block, shift);
    bigint_highest_digit(c, &block, &shift);
    printf("C highest {block, shift} = { %lu, %lu }\n", block, shift);
    
    if(bigint_g(a, b))
        printf("A is greater than B\n");
    else
        printf("A is less than B\n");

    bigint_free(&a);
    bigint_free(&b);
    bigint_free(&c);


/*    char in[1024];
    char out[1024];
    size_t i;
    int done = 0;
    
    while(1) {
        scanf("%1023s", in);
        for(i = 0; in[i]; i++) {
            if((in[i] < '0' || in[i] > '9') && in[i] != '-') {
                done = 1;
                break;
            }
        }
        if(done)
            break;
        BigInt *a = bigint(in);
        bigint_str(a, out, 1023);
        printf("output :%s:\n", out);

        bigint_free(&a);
    } */

    
    puts("Goodbye World!");
    return 0;
}
