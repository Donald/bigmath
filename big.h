#ifndef BIG_H
#define BIG_H

#include <stddef.h>

typedef struct BigInt BigInt;
BigInt *bigint(const char *str);
void    bigint_free(BigInt **bi);

void    bigint_highest_digit(BigInt *bi, size_t *block, size_t *shift);

int     bigint_g(BigInt *lh, BigInt *rh);
int     bigint_l(BigInt *lh, BigInt *rh);
int     bigint_e(BigInt *lh, BigInt *rh);
int     bigint_ne(BigInt *lh, BigInt *rh);

BigInt *bigint_add(BigInt *lh, BigInt *rh);
BigInt *bigint_sub(BigInt *lh, BigInt *rh);
BigInt *bigint_mul(BigInt *lh, BigInt *rh);
BigInt *bigint_div(BigInt *lh, BigInt *rh);

void    bigint_str(const BigInt *bi, char *dst, size_t max);

//typedef struct BigDec BigDec;

#endif
