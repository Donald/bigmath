CC = gcc
COMPILE = -I../include -O3 -Wall -Wpedantic -Werror -Wno-unused -std=c11
LINKER = 
TARGET = run

SOURCES = $(wildcard *.c)
OBJECTS = $(patsubst %.c,%.o,$(SOURCES))

all: $(OBJECTS) $(TARGET) clean

$(OBJECTS): $(SOURCES)
	$(CC) -c $(SOURCES) $(COMPILE)

$(TARGET): $(OBJECTS)
	$(CC) -o $(TARGET) $(OBJECTS) $(LINKER)

clean:
	rm *.o
