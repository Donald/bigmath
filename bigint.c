#include "big.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>
#include <assert.h>

struct BigInt {
    uint64_t *blocks;
    size_t   nblocks;
    int       positive;
    size_t    highblock;
    size_t    highshift;
};

static void highest_digit(BigInt *bi) {
    size_t b, s;
    for(b = bi->nblocks; b --> 0;) {
        s = 64;
        while(s > 0) {
            if(((bi->blocks[b] >> s) & 0xF) != 0) {
                bi->highblock = b;
                bi->highshift = ((s / 4) * 4) - 4;
                return;
            }
            s -= 4;
        } while(s > 0);
    }
    bi->highblock = 0;
    bi->highshift = 0;
}

BigInt *bigint(const char *str) {
    int positive;
    const char *n;
    if(str[0] != '-') {
        positive = 1;
        n = str;
    }
    else {
        positive = 0;
        n = str + 1;
    }
    
    size_t len     = strlen(n);
    size_t bits    = len * 8 / 2; /* Every character is reduced from 8 bits to 4 bits */
    size_t nblocks = ((bits + 63) / 64);// * 64; 
    printf("CHARACTERS = %lu = DIGITS = %lu = BLOCKS %lu\n", len, len, nblocks);

    BigInt *bi = malloc(sizeof(BigInt));
    bi->blocks   = calloc(nblocks, sizeof(uint64_t));
    bi->nblocks  = nblocks;
    bi->positive = positive;
    
    size_t i, b = 0, s = 0;
    for(i = len; i --> 0;) {
        bi->blocks[b] |= (uint64_t)((n[i] - '0') & 0xF) << s;
        s += 4;
        if(s % 64 == 0) {
            s = 0;
            b++;
        }
    }
   
    /* Lazy rn */
    highest_digit(bi);

    return bi;
}

void bigint_free(BigInt **bi) {
    if(bi == NULL || *bi == NULL)
        return;
    free((*bi)->blocks);
    free(*bi);
}

void bigint_highest_digit(BigInt *bi, size_t *block, size_t *shift) {
    if(block != NULL) *block = bi->highblock;
    if(shift != NULL) *shift = bi->highshift;
}


int bigint_g(BigInt *lh, BigInt *rh) {
    if(lh->nblocks > rh->nblocks)
        return 1;
    if(lh->nblocks < rh->nblocks)
        return 0;
    size_t b, s;
    uint8_t l, r;
    for(b = lh->nblocks; b --> 0;) {
        s = 64;
        while(s > 0) {
            l = ((lh->blocks[b] >> s) & 0xF);
            r = ((rh->blocks[b] >> s) & 0xF);
            if(!l && r)
                return 0;
            if(l && !r)
                return 1;
            s -= 4;
        }
    }
    return 0;
}

int bigint_l(BigInt *lh, BigInt *rh) {
    return !bigint_g(lh, rh);
}

int bigint_e(BigInt *lh, BigInt *rh) {
    return 0;
}

int bigint_ne(BigInt *lh, BigInt *rh) {
    return 0;
}


BigInt *bigint_add(BigInt *lh, BigInt *rh) {
    /*
     *  l  +  r   =   l  +  r
     *  l  + -r   =   l  -  r
     * -l  +  r   =   r  -  l
     * -l  + -r   = -(l  +  r)
     */
    if(lh->positive && !rh->positive)
        return bigint_sub(lh, rh);
    else if(!lh->positive && rh->positive)
        return bigint_sub(rh, lh);

    if(bigint_g(rh, lh)) {
        BigInt *t = rh;
        rh = lh;
        lh = t;
        puts("SWAP!");
    }

    BigInt *res = malloc(sizeof(BigInt));
    res->nblocks = lh->nblocks;
    res->blocks = calloc(res->nblocks, sizeof(uint64_t));
    printf("Start res blocks %lu\n", res->nblocks);
    printf("LH blocks %lu\nRH blocks %lu\n", lh->nblocks, rh->nblocks);

    uint8_t l, r, a, c = 0;
    size_t b, s;
    for(b = 0; b < res->nblocks; b++) {
        for(s = 0; s < 64; s += 4) {
            l = (lh->blocks[b] >> s) & 0xF;
            r = b >= rh->nblocks ? 0 : (rh->blocks[b] >> s) & 0xF;
            a = l + r + c;
            if(a >= 10) {
                a -= 10;
                c = 1;
            }
            else c = 0;
            res->blocks[b] |= (uint64_t)(a) << s;
        }
    }
    if(c) {
        res->nblocks++;
        res->blocks = realloc(res->blocks, sizeof(uint64_t) * res->nblocks);
        res->blocks[res->nblocks - 1] = 0x0000000000000001ll;
    }
    
    printf("Exiting LH + RH b/s = %lu/%lu\n", b, s);

    res->positive = (lh->positive && rh->positive);
    highest_digit(res);

    return res;
}

BigInt *bigint_sub(BigInt *lh, BigInt *rh) {
    /*
     *   l   -   r    =    l   -   r
     *   l   -  -r    =    l   +   r
     *  -l   -   r    =  -(l   +   r)
     *  -l   -  -r    =   -l   +   r    =    r   -   l
     */
    

    return NULL;
}

BigInt *bigint_mul(BigInt *lh, BigInt *rh) {
    return NULL;
}

BigInt *bigint_div(BigInt *lh, BigInt *rh) {
    return NULL;
}

void bigint_str(const BigInt *bi, char *dst, size_t max) {
    if(!bi->positive) {
        dst[0] = '-';
        dst[1] = '\0';
    }
    else
        dst[0] = '\0';

    size_t b;
    char buf[32];
    for(b = bi->nblocks; b --> 0;) {
        sprintf(buf, (b == bi->nblocks - 1) ? "%lx" : "%016lx", bi->blocks[b]); 
        strcat(dst, buf);
    }
}








